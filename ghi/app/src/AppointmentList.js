import React, { useEffect, useState } from 'react';


function AppointmentApp() {

  const [appointments, setAppointments] = useState([]);
  async function loadAppointments() {
    const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  }

  useEffect(() => {
    loadAppointments();
  }, []);

  function formatDateTime(dateTime) {
    const dateObj = new Date(dateTime);
    const month = String(dateObj.getMonth() + 1).padStart(2, '0');
    const day = String(dateObj.getDate()).padStart(2, '0');
    const year = dateObj.getFullYear();
    let hours = String(dateObj.getHours()).padStart(2, '0');
    const minutes = String(dateObj.getMinutes()).padStart(2, '0');
    const ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12 || 12;

    return `${month}/${day}/${year} ${hours}:${minutes}${ampm}`;
  }


  async function handleCancelAppointment(href) {
    const appointmentUrl = `http://localhost:8080${href}/cancel/`;
    const fetchOptions = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const Response = await fetch(appointmentUrl, fetchOptions);
    if (Response.ok) {
      loadAppointments();
    }
  };


  async function handleFinishAppointment(href) {
    const appointmentUrl = `http://localhost:8080${href}/finish/`;
    const fetchOptions = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const Response = await fetch(appointmentUrl, fetchOptions);
    if (Response.ok) {
      loadAppointments();
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Is Vip?</th>
          <th>Customer Name</th>
          <th>Date & Time</th>
          <th>Technician</th>
          <th>Reason</th>
        </tr>
      </thead>
      <tbody>
        {appointments && appointments.map(appointments => {
          return (
            <tr key={appointments.vin}>
              <td>{appointments.vin}</td>
              <td>{appointments.vip}</td>
              <td>{appointments.customer}</td>
              <td>{formatDateTime(appointments.date_time)}</td>
              <td>{appointments.technician}</td>
              <td>{appointments.reason}</td>
              <td>
                <button className="btn btn-sm btn-danger"
                  onClick={() => handleCancelAppointment(appointments.href)}>
                  Cancel
                </button>
                <button className="btn btn-sm btn-primary"
                  onClick={() => handleFinishAppointment(appointments.href)}>
                  Finish
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AppointmentApp;
