from django.shortcuts import render
from .models import Technician, Appointment
import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .encoder import (
    AppointmentDetailEncoder,
    AppointmentListEncoder,
    TechnicianListEncoder,
)

# Create your views here.


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(technician, encoder=TechnicianListEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_technicians(request, id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=id)
            return JsonResponse(technician, encoder=TechnicianListEncoder, safe=False)
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"Deleted": count > 0})
    else:
        content = json.loads(request.body)
        Technician.objects.filter(id=id).update(**content)
        technician = Technician.objects.get(id=id)
        return JsonResponse(technician, encoder=TechnicianListEncoder, safe=False)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments}, encoder=AppointmentListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            employee_id = content["technician"]
            technician = Technician.objects.get(employee_id=employee_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid technician id"}, status=400)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_show_appointments(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment, encoder=AppointmentDetailEncoder, safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Invalid appointment id"}, status=400)
    else:
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"Deleted": count > 0})


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.status = "cancelled"
    appointment.save()
    return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)


@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.status = "finished"
    appointment.save()
    return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)
