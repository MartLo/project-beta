import React, { useState } from 'react';

function TechForm() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeID, setEmployeeID] = useState('');
  const [hasSignedUp, setHasSignedUp] = useState(false);


  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employeeID;

    const techUrl = 'http://localhost:8080/api/technicians/';
    const fetchOptions = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const techResponse = await fetch(techUrl, fetchOptions);
    if (techResponse.ok) {
      setFirstName('');
      setLastName('');
      setEmployeeID('');
      setHasSignedUp(true);
    }
  }

  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleEmployeeIDChange = (event) => {
    const value = event.target.value;
    setEmployeeID(value);
  }
  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (hasSignedUp) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-tech-form">
                <h1 className="card-title">Add a Technician</h1>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleFirstNameChange} required placeholder="Your first name" type="text" id="first_name" name="first_name" className="form-control" />
                      <label htmlFor="name">Your first name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleLastNameChange} required placeholder="Your last name" type="text" id="last_name" name="last_name" className="form-control" />
                      <label htmlFor="color">Your last name</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleEmployeeIDChange} required placeholder="Your Employee ID" type="text" id="employee_id" name="employee_id" className="form-control" />
                      <label htmlFor="color">Your employee id</label>
                    </div>
                  </div>
                </div>

                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                Congratulations!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}


export default TechForm;
