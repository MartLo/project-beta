import { useState, useEffect } from "react";

function ModelsForm() {
  const [name, setName] = useState('');
  const [picture_url, setPictureUrl] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [manufacturers, setManufacturers] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.manufacturer = manufacturer
    data.name = name;
    data.picture_url = picture_url;
    const modelUrl = 'http://localhost:8100/api/models/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(modelUrl, fetchOptions);
    if (response.ok) {
      setManufacturer('');
      setName('');
      setPictureUrl('');
      setCreatedModel(true);
      setTimeout(() => setCreatedModel(false), 3000);
    }
  }

  const handleChangeManufacturer = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }

  const handleChangeName = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleChangePictureUrl = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  }

  const [createdModel, setCreatedModel] = useState(false);

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (manufacturers.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (createdModel) {
    messageClasses = 'alert alert-success mb-0';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-Model-form">
                <h1 className="card-title">Add a model</h1>
                <div className={spinnerClasses} id="loading-manufacturer-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={handleChangeManufacturer} name="manufacturer" id="manufacturer" className={dropdownClasses} required>
                    <option value="">Choose a manufacturer</option>
                    {manufacturers.map(manufacturer => {
                      return (
                        <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input onChange={handleChangeName} required placeholder="Automobile VIN" maxLength={17} type="text" id="name" name="name" className="form-control" />
                    <label htmlFor="name">Model name</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input onChange={handleChangePictureUrl} required placeholder="picture_url name" type="text" id="picture_url" name="picture_url" className="form-control" />
                    <label htmlFor="picture_url">picture_url</label>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                New model has been created!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModelsForm
