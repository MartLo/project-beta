import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechForm from './TechForm';
import TechList from './TechList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './ModelsList';
import ModelsForm from './ModelsForm';
import AutomobilesList from './AutomobilesList';
import AutomobilesForm from './AutomobilesForm';
import ServiceHistory from './ServiceHist';
import SalespersonForm from './SalespersonForm';
import ListSalesperson from './ListSalespeople';
// import ListSale from '/ListSale';
import SaleForm from './SaleForm';
import CustomerForm from './CustomerForm';
import ListCustomer from './ListCustomer';

function App(props) {
  if (props.technicians === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians">
            <Route index element={<TechList technicians={props.technicians}/>} />
            <Route path="new" element={<TechForm/>} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList/>} />
            <Route path="new" element={<AppointmentForm/>} />
            <Route path="history" element={<ServiceHistory/>} />
          </Route>
          <Route path="manufacturers" >
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models" >
            <Route index element={<ModelsList />} />
            <Route path="new" element={<ModelsForm />} />
          </Route>
          <Route path="automobiles" >
            <Route index element={<AutomobilesList />} />
            <Route path="new" element={<AutomobilesForm />} />
          </Route>
          <Route path="/" element={<MainPage />} />
          <Route path="salesperson">
            <Route index element={<ListSalesperson/>} />
            <Route path="new" element={<SalespersonForm/>} />
          </Route>
          <Route path="sales">
            {/* <Route index element={<ListSale/>} /> */}
            <Route path="new" element={<SaleForm/>} />
          </Route>
          <Route path="customers" >
            <Route index element={<ListCustomer />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
