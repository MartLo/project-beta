from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["date_time", "reason", "status", "vin", "customer"]

    def get_extra_data(self, o):
        return {"technician": o.technician.employee_id}


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = ["date_time", "reason", "status", "vin", "customer", "technician"]
    encoders = {
        "technician": TechnicianListEncoder(),
    }
