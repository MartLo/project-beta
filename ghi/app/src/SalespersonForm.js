import React, { useState } from "react";

function SalespersonForm() {
  const [formData, setData] = useState({
    first_name: "",
    last_name: "",
    employee_id: "",
  });

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setData({
      ...formData,
      [inputName]: value,
    });
  };
  const handleSubmit = async (event) => {
    event.preventDefault();

    const salespersonURL = 'http://localhost:8090/api/salesperson/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(salespersonURL, fetchConfig);
    if (response.ok) {
      setData({
        first_name: "",
        last_name: "",
        employee_id: "",
      });
    }
  }


  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-Customer-form">
                <h1 className="card-title">Create a Salesperson</h1>
                <div className="row">
                  <div className="form-floating mb-3">
                    <input
                      onChange={handleFormChange}
                      required
                      placeholder="First Name"
                      type="text"
                      id="first_name"
                      name="first_name"
                      className="form-control"
                      value={formData.first_name}
                    />
                    <label htmlFor="name">First Name *</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                      onChange={handleFormChange}
                      required
                      placeholder="Last Name"
                      type="text"
                      id="last_name"
                      name="last_name"
                      className="form-control"
                      value={formData.last_name}
                    />
                    <label htmlFor="name">Last Name *</label>
                    </div>
                  <div className="form-floating mb-3">
                    <input
                      onChange={handleFormChange}
                      required
                      placeholder="Employee ID"
                      type="text"
                      id="employee_id"
                      name="employee_id"
                      className="form-control"
                      value={formData.employee_id}
                    />
                    <label htmlFor="name">Employee ID *</label>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              <div id="success-message">Salesperson successfully created.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalespersonForm;
