import React, { useState, useEffect } from "react";

function SaleForm() {
  const [salesperson, setSalesperson] = useState([]);
  const [customer, setCustomer] = useState([]);
  const [automobile, setAutomobile] = useState([]);
  const [price, setPrice] = useState('')
  const [hasSignedUp, setHasSignedUp] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.automobile = automobile;
    data.salesperson = salesperson;
    data.customer = customer;
    data.price = price;


    const saleURL = 'http://localhost:8090/api/sales/';
    const fetchOptions = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    
    const techResponse = await fetch(saleURL, fetchOptions);
    if (techResponse.ok) {
      setAutomobile('');
      setSalesperson('');
      setCustomer('');
      setPrice('');
      setHasSignedUp(true);
    }
  }

  const handleAutomobileChange = (event) => {
    const value = event.target.value;
    setAutomobile(value);
  }

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  }

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  }
  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (hasSignedUp) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }
  const fetchSalesperson = async () => {
    const url = "http://localhost:8090/api/salesperson/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalesperson(data.salesperson);
    }
  }

  useEffect(() => {
    fetchSalesperson();
  }, []);

  const fetchCustomer = async () => {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setCustomer(data.customers);

    }
  }

  useEffect(() => {
    fetchCustomer();
  }, []);

  return (
    <>
      <div className="my-5 container">
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
                <form className={formClasses} onSubmit={handleSubmit} id="create-sale-form">
                  <h1 className="card-title">Create a Sale</h1>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleAutomobileChange} value={automobile} required placeholder="Automobile Vin" type="text" maxLength="17" id="vin" name="vin" className="form-control" />
                        <label htmlFor="automobile">Automobile VIN</label>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <select onChange={handleCustomerChange} value={customer} required id="customer" name="customer" className="form-select">
                          <option value="">Choose a Customer</option>
                          {customer.map((customer) => {
                            return (
                              <option key={customer.last_name} value={customer.last_name}>
                                {customer.last_name}
                              </option>
                            );
                          })}
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <select onChange={handleSalespersonChange} value={salesperson} required id="salesperson" name="salesperson" className="form-select">
                          <option value="">Choose a Salesperson</option>
                          {salesperson.map((salesperson) => {
                            return (
                              <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                {salesperson.employee_id}
                              </option>
                            );
                          })}
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handlePriceChange} required placeholder="Price" type="text" id="price" name="price" className="form-control" />
                        <label htmlFor="price">Price</label>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">Create</button>
                </form>
                <div className={messageClasses} id="success-message">
                  Your Sale has been created successfully.
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default SaleForm;
