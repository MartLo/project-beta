import React, { useState, useEffect } from "react";

function SalespersonList() {
    const [salespersons, setSalespersons] = useState([]);

    const fetchSalespersons = async () => {
        const salespersonURL = 'http://localhost:8090/api/salesperson/';
        const response = await fetch(salespersonURL);
        if (response.ok) {
            const data = await response.json();
            setSalespersons(data);
        }
    };


    useEffect(() => {
        fetchSalespersons();
    }, []);

    return (
        <div>
            <h2>Salespeople</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespersons.map((salesperson) => (
                    <tr key={salesperson.employee_id}>
                        <td>{salesperson.employee_id}</td>
                        <td>{salesperson.first_name}</td>
                        <td>{salesperson.last_name}</td>
                    </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default SalespersonList;
