from django.urls import path
from sales_rest.views import (
    list_sales,
    show_sales,
    list_customer,
    show_customer,
    list_salesperson,
    show_salesperson,
)

urlpatterns = [
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:pk>/", show_sales, name="show_sales"),
    path("customers/", list_customer, name="list_customer"),
    path("customers/<int:pk>/", show_customer, name="show_customer"),
    path("salesperson/", list_salesperson, name="list_salesperson"),
    path("salesperson/<int:pk>/", show_salesperson, name="show_salesperson"),
]
