import React, { useState, useEffect } from 'react';

function AppointmentForm() {
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [dateTime, setDateTime] = useState('');
  const [technician, setTechnician] = useState('');
  const [technicians, setTechnicians] = useState([]);
  const [reason, setReason] = useState('');
  const [hasSignedUp, setHasSignedUp] = useState(false);


  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.vin = vin;
    data.customer = customer;
    data.date_time = dateTime;
    data.technician = technician;
    data.reason = reason;

    const appointmentUrl = 'http://localhost:8080/api/appointments/';
    const fetchOptions = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const techResponse = await fetch(appointmentUrl, fetchOptions);
    if (techResponse.ok) {
      setVin('');
      setCustomer('');
      setDateTime('');
      setTechnician('');
      setReason('');
      setHasSignedUp(true);
    }
  }

  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  }

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const handleDateTimeChange = (event) => {
    const value = event.target.value;
    setDateTime(value);
  }
  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  }
  const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
  }
  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (hasSignedUp) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }
  const fetchData = async () => {
    const url = "http://localhost:8080/api/technicians/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-appointment-form">
                <h1 className="card-title">Create a Service Appointment</h1>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleVinChange} value={vin} required placeholder="Automobile Vin" type="text" maxLength="17" id="vin" name="vin" className="form-control" />
                      <label htmlFor="vin">Automobile Vin</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleCustomerChange} value={customer} required placeholder="Customer" type="text" id="customer" name="customer" className="form-control" />
                      <label htmlFor="customer">Customer</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleDateTimeChange} value={dateTime} required placeholder="Date" type="datetime-local" id="date_time" name="date_time" className="form-control" />
                      <label htmlFor="date_time">Date and Time</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <select onChange={handleTechnicianChange} value={technician} required id="technician" name="technician" className="form-select">
                        <option value="">Choose a technician</option>
                        {technicians.map(technician => {
                          return (
                            <option key={technician.employee_id} value={technician.employee_id}>
                              {technician.employee_id}
                            </option>
                          );
                        })}
                      </select>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleReasonChange} required placeholder="Reason" type="text" id="reason" name="reason" className="form-control" />
                      <label htmlFor="reason">Reason</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                Your service appointment has been made!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}


export default AppointmentForm;
