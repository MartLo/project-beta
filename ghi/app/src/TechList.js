function TechnicianApp(props) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Employee ID</th>
          <th>First Name</th>
          <th>Last Name</th>
        </tr>
      </thead>
      <tbody>
        {props.technicians && props.technicians.map(technicians => {
          return (
            <tr key={technicians.employee_id}>
              <td>{technicians.employee_id}</td>
              <td>{technicians.first_name}</td>
              <td>{technicians.last_name}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default TechnicianApp;
