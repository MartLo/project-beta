import React, { useState } from "react";

function CustomerForm() {
  const [formData, setData] = useState({
    first_name: "",
    last_name: "",
    address: "",
    phone_number: "",
  });

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setData({
      ...formData,
      [inputName]: value,
    });
  };
  const handleSubmit = async (event) => {
    event.preventDefault();

    const customerURL = 'http://localhost:8090/api/customers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(customerURL, fetchConfig);
    if (response.ok) {
      setData({
        first_name: "",
      last_name: "",
      address: "",
      phone_number: "",
      });
    }
  }


  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-Customer-form">
                <h1 className="card-title">Create a Customer</h1>
                <div className="row">
                  <div className="form-floating mb-3">
                    <input
                      onChange={handleFormChange}
                      required
                      placeholder="First Name"
                      type="text"
                      id="first_name"
                      name="first_name"
                      className="form-control"
                      value={formData.first_name}
                    />
                    <label htmlFor="name">First Name *</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                      onChange={handleFormChange}
                      required
                      placeholder="Last Name"
                      type="text"
                      id="last_name"
                      name="last_name"
                      className="form-control"
                      value={formData.last_name}
                    />
                    <label htmlFor="name">Last Name *</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                      onChange={handleFormChange}
                      required
                      placeholder="Address"
                      type="text"
                      id="address"
                      name="address"
                      className="form-control"
                      value={formData.address}
                    />
                    <label htmlFor="name">Address *</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                      onChange={handleFormChange}
                      required
                      placeholder="Phone Number"
                      type="text"
                      id="phone_number"
                      name="phone_number"
                      className="form-control"
                      value={formData.phone_number}
                    />
                    <label htmlFor="name">Phone Number *</label>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              <div id="success-message">Customer successfully created.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CustomerForm;
