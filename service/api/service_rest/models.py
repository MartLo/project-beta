from django.db import models
from django.urls import reverse


# Create your models here.
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=50, null=True)

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"id": self.id})

    def __str__(self):
        return self.employee_id


class Appointment(models.Model):
    STATUS_CHOICES = [
        ("finished", "Finished"),
        ("canceled", "Canceled"),
        ("created", "Created"),
    ]
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, null=True)
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=100)
    vip = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"id": self.id})
