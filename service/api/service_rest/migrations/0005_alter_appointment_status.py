# Generated by Django 4.0.3 on 2023-07-26 14:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("service_rest", "0004_alter_appointment_date_time"),
    ]

    operations = [
        migrations.AlterField(
            model_name="appointment",
            name="status",
            field=models.CharField(
                choices=[("finished", "Finished"), ("cancelled", "Cancelled")],
                default="finished",
                max_length=50,
            ),
        ),
    ]
