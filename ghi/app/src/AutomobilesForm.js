import { useState, useEffect } from "react";

function AutomobilesForm() {
  const [color, setColor] = useState('');
  const [year, setYear] = useState('');
  const [vin, setVin] = useState('');
  const [model, setModel] = useState('');
  const [models, setModels] = useState([]);
  const [createdAutomobile, setCreatedAutomobile] = useState(false);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.model = model
    data.color = color;
    data.vin = vin;
    data.year = year;
    const autoUrl = 'http://localhost:8100/api/automobiles/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const autoResponse = await fetch(autoUrl, fetchOptions);
    if (autoResponse.ok) {
      setModel('');
      setColor('');
      setVin('');
      setYear('');
      setCreatedAutomobile(true);
    }
  }

  const handleChangeModel = (event) => {
    const value = event.target.value;
    setModel(value);
  }

  const handleChangeColor = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const handleChangeVin = (event) => {
    const value = event.target.value;
    setVin(value);
  }


  const handleChangeYear = (event) => {
    const value = event.target.value;
    setYear(value);
  }

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (models.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (createdAutomobile) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-automobile-form">
                <h1 className="card-title">Add an automobile</h1>
                <p className="mb-3">
                  Assign a model
                </p>
                <div className={spinnerClasses} id="loading-model-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>

                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeColor} required placeholder="color" maxLength={17} type="text" id="color" name="color" className="form-control" />
                      <label htmlFor="color">Color...</label>
                    </div>
                  </div>

                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeYear} required placeholder="year" type="text" id="year" name="year" className="form-control" />
                      <label htmlFor="year">Year...</label>
                    </div>
                    <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeVin} required placeholder="vin" type="vintime-local" maxLength="17" id="vin" name="vin" className="form-control" />
                      <label htmlFor="vin">VIN...</label>
                    </div>
                  </div>
                  </div>
                  <div className="mb-3">
                  <select onChange={handleChangeModel} name="model" id="model" className={dropdownClasses} required>
                    <option value="">Choose a model...</option>
                    {models.map(model => {
                      return (
                        <option key={model.id} value={model.id}>{model.name}</option>
                      )
                    })}
                  </select>
                </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                Automobile has been added.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AutomobilesForm
