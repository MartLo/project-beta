import React, { useState, useEffect } from "react";

function CustomerList() {
    const [customer, setCustomer] = useState([]);

    const fetchCustomer = async () => {
        const customerURL = 'http://localhost:8090/api/customers/';
        const response = await fetch(customerURL);
        if (response.ok) {
            const data = await response.json();
            setCustomer(data);
        }
    };


    useEffect(() => {
        fetchCustomer();
    }, []);

    return (
        <div>
            <h2>Customers</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Last Name</th>
                        <th>First Name</th>
                        <th>Phone Number</th>
                    </tr>
                </thead>
                <tbody>
                    {customer.map((salesperson) => (
                    <tr key={customer.last_name}>
                        <td>{customer.first_name}</td>
                        <td>{customer.last_name}</td>
                        <td>{customer.phone_number}</td>
                    </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default CustomerList;
