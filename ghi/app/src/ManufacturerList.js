import React, { useEffect, useState } from "react";

function ManufacturerList() {
  const [manufacturers, setManufacturers] = useState([])

  async function loadManufacturers() {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }

  useEffect(() => {
    loadManufacturers();
  }, []);



  return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers && manufacturers.map(manufacturers => {
            return (
              <tr className="table-info" key={manufacturers.href} value={manufacturers.href}>
                <td>{manufacturers.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
  )



}

export default ManufacturerList
