from django.db import models
from django.urls import reverse
from django.core.validators import RegexValidator


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50, null=True)
    employee_id = models.CharField(max_length=30, unique=True)


class Customer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50, null=True)
    address = models.CharField(max_length=100, null=True)
    phone_number = models.CharField(
        max_length=20,
        validators=[RegexValidator(r'^\d{1,20}$')]
    )

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.CASCADE,
        )
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    price = models.PositiveSmallIntegerField(primary_key=True)

    def __str__(self):
        return f"Sale: {self.automobile.vin} - {self.salesperson.first_name} {self.salesperson.last_name} - {self.customer.first_name} {self.customer.last_name}"

    def get_api_url(self):
        return reverse("show_sales", kwargs={"pk": self.pk})
