from .models import Sale, Customer, AutomobileVO, Salesperson
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from sales_rest.encoders import (
    SalespersonListEncoder,
    CustomerListEncoder,
    SaleListEncoder,
)


@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": list(sales)},
            encoder=SaleListEncoder,
            safe=False,
            content_type="application/json"
        )
    else:
        content = json.loads(request.body)
        try:
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile id"},
                status=400
            )
        sales = Sale.objects.create(**content)
        return JsonResponse(
            sales,
            encoder=SaleListEncoder,
            safe=False,
            content_type="application/json"
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_sales(request, pk):
    if request.method == "GET":
        try:
            sales = Sale.objects.get(id=pk)
            return JsonResponse(
                sales,
                encoder=SaleListEncoder,
                safe=False,
                content_type="application/json"
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Sale.objects.filter(id=pk).delete()

            return JsonResponse(
                {"deleted": count > 0},
                content_type="application/json"
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            sales = Sale.objects.get(id=pk)

            props = [
                "automobile",
                "salesperson",
                "customer",
                "price",
            ]
            for prop in props:
                if prop in content:
                    setattr(sales, prop, content[prop])
            sales.save()
            return JsonResponse(
                sales,
                encoder=SaleListEncoder,
                safe=False,
                content_type="application/json"
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_salesperson(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonListEncoder,
            safe=False,
            content_type="application/json"
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonListEncoder,
            safe=False,
            content_type="application/json"
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_salesperson(request, pk):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salesperson,
                encoder=SalespersonListEncoder,
                safe=False,
                content_type="application/json"
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Salesperson.objects.filter(id=pk).delete()

            return JsonResponse(
                {"deleted": count > 0},
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.get(id=pk)

            props = [
                "first_name",
                "last_name",
                "employee_id",
            ]
            for prop in props:
                if prop in content:
                    setattr(salesperson, prop, content[prop])
            salesperson.save()
            return JsonResponse(
                salesperson,
                encoder=SalespersonListEncoder,
                safe=False,
                content_type="application/json"
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
            safe=False,
            content_type="application/json"
        )
    else:
        content = json.loads(request.body)
        customers = Customer.objects.create(**content)
        return JsonResponse(
            customers,
            encoder=CustomerListEncoder,
            safe=False,
            content_type="application/json"
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_customer(request, pk):
    if request.method == "GET":
        try:
            customers = Customer.objects.get(id=pk)
            return JsonResponse(
                customers,
                encoder=CustomerListEncoder,
                safe=False,
                content_type="application/json"
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Customer.objects.filter(id=pk).delete()

            return JsonResponse(
                {"deleted": count > 0},
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            customers = Customer.objects.get(id=pk)

            props = [
                "first_name",
                "last_name",
                "phone_number",
            ]
            for prop in props:
                if prop in content:
                    setattr(customers, prop, content[prop])
            customers.save()
            return JsonResponse(
                customers,
                encoder=CustomerListEncoder,
                safe=False,
                content_type="application/json"
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
